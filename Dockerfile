FROM openjdk:18-jdk-alpine3.15
VOLUME /tmp
COPY .mvn .mvn
COPY pom.xml .
COPY src src
COPY target/*.jar /root/.m2/repository/io/helyx/mds-dds-exam-spring-boot-server/0.0.1-SNAPSHOT/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar", "-Dserver.port=$PORT","/root/.m2/repository/io/helyx/mds-dds-exam-spring-boot-server/0.0.1-SNAPSHOT/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar"]
